CREATE TABLE "sales_data" (
  "Date" timestamp,
  "Product_ID" int,
  "Product_Name" varchar(100),
  "sum_quantity" float,
  "sum_amount" float
);

COMMIT;