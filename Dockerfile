FROM postgres
ENV POSTGRES_PASSWORD docker
ENV POSTGRES_DB pepsico
COPY init.sql /docker-entrypoint-initdb.d/
