import pandas as pd
from pyspark.sql import SparkSession, dataframe, session
import pyspark.sql.functions as F
from openpyxl import Workbook


def get_spark_session() -> session.SparkSession: 
    spark = SparkSession.builder\
        .config("spark.driver.extraClassPath", "postgresql-42.7.3.jar")\
        .appName("Main")\
        .getOrCreate()

    return spark


def get_df() -> pd.DataFrame:
    df = pd.read_excel('Source/DetailSales.xlsx')

    # Date column to datetime format for all rows
    df['Fecha'] = pd.to_datetime(df['Fecha'])

    return df


def write_to_pqt(df: dataframe.DataFrame, path: str, name: str) -> None:
    try:
        df.write.format('parquet').save(f'{path}/{name}.pqt')
        print("[Succes] while write spark df to parquet")
    except Exception as e:
        print(f"[Error] while write spark df to parquet. Error {e}")
    

def save_pyspark_to_xlsx(spark_df: dataframe.DataFrame, name: str, path:str) -> None:
    workbook = Workbook()
    worksheet = workbook.active

    columns = spark_df.columns
    for i, column_name in enumerate(columns, start=1):
        worksheet.cell(row=1, column=i, value=column_name)

    for row_index, row in enumerate(spark_df.collect(), start=2):
        for col_index, cell_value in enumerate(row, start=1):
            worksheet.cell(row=row_index, column=col_index, value=str(cell_value))

    try:
        workbook.save(f'{path}/{name}.xlsx')
        print("[Succes] while saving spark df as xlsx")
    except Exception as e:
        print(f"[Error] while saving spark df as xlsx. Error: {e}")


def write_pyspark_to_csv(df: dataframe.DataFrame, path: str, name: str) -> None:
    try:
        df.write.options(header=True, delimiter=',').csv(f'{path}/{name}')
        print("[Succes] while saving spark df as csv")
    except Exception as e:
        print(f"[Error] while saving spark df as csv. Error: {e}")

    

def process_data(spark_df: dataframe.DataFrame) -> dataframe.DataFrame:
    # Pattern for data exctracting
    pattern = r'(?:.*)YEAR=(\d+).+?MONTH=(\d+).+?DAY_OF_MONTH=(\d+).+'
    df = spark_df\
        .withColumnRenamed('Producto', 'Product_ID')\
        .withColumnRenamed('Nombre', 'Product_Name')\
        .withColumnRenamed('Cantidad', 'Quantity')\
        .withColumnRenamed('Precio', 'Amount')\
        .withColumnRenamed('Fecha', 'Date')\
        .withColumn('Date', F.regexp_replace('Date', pattern, '$1-$2-$3').cast('timestamp'))\
        .groupBy('Date', 'Product_ID', 'Product_Name')\
            .sum('Quantity', 'Amount')\
        .withColumn('Amount', F.col('sum(Amount)')*75.75)
    
    df = df.drop('sum(Amount)').withColumnRenamed('sum(Quantity)', 'Quantity')

    return df


def write_data_to_postgres(df: dataframe.DataFrame) -> None:
    try:
        df.write.format('jdbc')\
            .option('url', 'jdbc:postgresql://localhost:5432/pepsico')\
            .option('driver', 'org.postgresql.Driver')\
            .option('dbtable', 'sales_data')\
            .option('user', 'postgres')\
            .option('password', 'docker')\
            .save(mode='append')
        print("[Succes] data was saved in DB.")
    except Exception as e:
        print(f'[ERROR] data was not saved in DB. Error: {e}')


def main():
    # Start spark session
    spark = get_spark_session()
    
    # Import data from xlsx file
    spark_df = spark.createDataFrame(get_df())

    # Move dataframe to Stage folder
    write_to_pqt(df=spark_df, path='Stage', name='DetailSales')

    # Move data from spark df to Archive
    save_pyspark_to_xlsx(spark_df=spark_df, path='Archive', name='DetailSales')

    # Execute all needed manipulations with data
    processed_df = process_data(spark_df=spark_df)

    # Save new df to Business folder
    write_pyspark_to_csv(df=processed_df, path='Business', name='Sales')

    # Write data to Postgres DB
    write_data_to_postgres(df=processed_df)

    # End pyspark session
    spark.stop()



if __name__ == '__main__':
    main()